A 'better' modified logging class for pyrocms, custom error levels also logging only INFO tags and not others if you wish.

Got this in the controller:

$this->config->load('config');
$this->load->library('my_log');
		
got the config.php file in the config dir, and in it: 

$config['show_in_log'] = array('INFO');
$config['log_path'] = "system/cms/logs/";

for example.

also: http://ellislab.com/forums/viewthread/83716/

and / or / perhaps

https://github.com/EllisLab/CodeIgniter/wiki/MY-Log